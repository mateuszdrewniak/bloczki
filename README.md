# Bloczki

Bloczki to epicki serwer Minecraft na Fabric z modami.

Adres serwera to: `bloczki.bh-games.com`

Paczka modów: https://gitlab.com/mateuszdrewniak/bloczki/-/archive/main/bloczki-main.zip (mody w folderze `mods`)

## Spis treści

[[_TOC_]]

## Lista modów

Lista modów dostępna jest [tutaj](modlist.md).

## Aktualizacja modów

1. Wejdź do menu głównego Minecraft i wybierz opcję **Mods**

    ![Otwórz **Mods**](assets/open_mods_menu.png)

1. Kliknij na **Open mods folder** aby otworzyc folder z modyfikacjami Minecraft.

    ![Otwórz **Mods**](assets/open_mods_folder.png)

1. Przejdź do katalogu głównego minecraft (z otwartego folderu `mods` przejdź do jego folderu nadrzędnego)

1. Pobierz [paczkę modów](https://gitlab.com/mateuszdrewniak/bloczki/-/archive/main/bloczki-main.zip) serwera *Bloczki*.

1. Rozpakuj pobrane archiwum `.zip` z paczką modów

1. Skopiuj folder `mods` z paczki modów do wcześniej otwartego folderu głównego Minecraft (zastap istniejace pliki)

1. Wrzuć zawartość folderu `resourcepacks` z paczki modów do wcześniej otwartego folderu głównego Minecraft

1. Zrestartuj Minecraft


1. Wejdź w zakładkę **Options**.

    ![Przycisk **Options**](assets/options_button.png)

1. Wejdź w zakładkę **Resource Packs**.

    ![Przycisk **Resource Packs**](assets/resource_packs_button.png)

1. Włacz pokazane na obrazku resource packi.

    ![Wymagane **Resource Packs**](assets/resource_packs.png)

## Przygotowanie Minecraft do gry

1. Zainstaluj swój preferowany launcher Minecraft
    - Sugeruję [TLauncher](https://tlauncher.org/en/)

1. Uruchom launcher i zainstaluj Minecraft **Fabric 1.21.1**.
1. Wejdź do gry, stwórz jeden świat i wyjdź z gry po jego załadowaniu

1. Wejdź do folderu głównego minecraft

    - Windows: `%APPDATA%\.minecraft`
    - MacOS: `~/Library/Application Support/minecraft`
    - Linux: `~/.minecraft`

1. Pobierz [paczkę modów](https://gitlab.com/mateuszdrewniak/bloczki/-/archive/main/bloczki-main.zip) serwera *Bloczki*.

1. Rozpakuj pobrane archiwum `.zip` z paczką modów i wrzuć foldery `mods` i `resourcepacks` do wcześniej otwartego folderu głównego Minecraft

1. Zrestartuj Minecraft i upewnij się, że poprawnie się włącza. Menu główne powinno wyglądac mniej więcej tak.

    ![Menu główne](assets/main_menu.png)

1. Wejdź w zakładkę **Options**.

    ![Przycisk **Options**](assets/options_button.png)

1. Wejdź w zakładkę **Resource Packs**.

    ![Przycisk **Resource Packs**](assets/resource_packs_button.png)

1. Włacz pokazane na obrazku resource packi.

    ![Wymagane **Resource Packs**](assets/resource_packs.png)

1. Wróć do Menu głównego i wejdź w zakładkę **Multiplayer**.

    ![Przycisk **Multiplayer**](assets/multiplayer_menu_button.png)

1. Kliknij **Add Server**.

    ![Przycisk **Add Server**](assets/add_server_button.png)

1. Wpisz dane serwera i dodaj go (adres: `bloczki.bh-games.com`)

    ![Dodawanie serwera](assets/add_server_menu.png)

1. W menu multiplayer powinien by teraz dostępny serwer **Bloczki**. Zielona fajka oznacza, że Minecraft został poprawnie skonfigurowany do gry na tym serwerze.

    ![Menu multiplayer](assets/server_list.png)

<!-- ## Dostosowanie skina

Skina można dostosowac za pomocą [Ely.by](https://ely.by/). -->
